﻿jQuery(document).ready(function() {
// BEGIN of script for top-slider
var	topSlider = $('.top-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// adaptiveHeight: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		fade: true,
		pauseOnHover: false,
		speed: 1000,
		dots: true
	});
// END of script for top-slider


// BEGIN of script for sidebar-sliders
var sidebarSlider1 = $('#sidebarSlider1').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// adaptiveHeight: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: true
	});
$('.next-btn').click(function(){
 	$(sidebarSlider1).slick("slickNext");
 });
 $('.prev-btn').click(function(){
 	$(sidebarSlider1).slick("slickPrev");
 });




 var sidebarSlider2 = $('#sidebarSlider2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// adaptiveHeight: true,
		// autoplay: true,
		// autoplaySpeed: 3000,
		pauseOnHover: false,
		speed: 1000,
		dots: true
	});
$('.next-btn').click(function(){
 	$(sidebarSlider1).slick("slickNext");
 });
 $('.prev-btn').click(function(){
 	$(sidebarSlider1).slick("slickPrev");
 });


// END of script for top-slider

// FansyBox
$('[data-fancybox="gallery"]').fancybox({
	// Options will go here
});




// BEGIN of script to stick header


    $(window).scroll(function(){
    	var headerHeight = $("header").height();
        var sticky = $('.main-menu__wrapper'),
            scroll = $(window).scrollTop();
        if (scroll > headerHeight) {
            sticky.addClass('header-fixed');
        } else {
            sticky.removeClass('header-fixed');
            $('.top-slider').slick('setPosition'); 
        };
    });

// END of script to stick header


jQuery.validator.addMethod("checkMask", function(value, element) {
     return /\+\d{1}\(\d{3}\)\d{3}-\d{4}/g.test(value); 
});

// Begin of validate 
$('.delivery-form').validate({
	rules: {
		name: {
			required: true,
		},
		phone: {
			required: true,
			checkMask: true,
		},
	},
	errorPlacement: function() {
        return false;
    }
});


$('.phone-mask').mask("+7(999)999-9999");

$(".delivery-form__item input").blur(function(){
    if ($(this).hasClass('valid')) {
	    $(this).parent().toggleClass('valid-input');
	}
	if ($(this).hasClass('error')) {
	    $(this).parent().removeClass('valid-input');
	}
});



});